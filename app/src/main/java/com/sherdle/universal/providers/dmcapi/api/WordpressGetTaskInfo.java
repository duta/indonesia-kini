package com.sherdle.universal.providers.dmcapi.api;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;

import com.sherdle.universal.providers.dmcapi.PostItem;
import com.sherdle.universal.providers.dmcapi.DmcListAdapter;
import com.sherdle.universal.providers.dmcapi.api.providers.JetPackProvider;
import com.sherdle.universal.providers.dmcapi.api.providers.JsonApiProvider;
import com.sherdle.universal.providers.dmcapi.api.providers.WordpressProvider;

import java.util.ArrayList;

public class WordpressGetTaskInfo{

    //Paging and status
	public Integer pages;
	public Integer curpage = 0;
    public boolean isLoading;

    public DmcListAdapter adapter = null;
	public ArrayList<PostItem> posts;

    //Static information about this instance
	public String baseurl;
	public Boolean simpleMode;
	public WordpressProvider provider = null;
	public Long ignoreId = 0L; //ID of post not to add

    //Views to track
    public RecyclerView listView = null;

    //References
    public Activity context;
    private ListListener listener;
	
	public WordpressGetTaskInfo(RecyclerView listView,
								Activity context,
								String baseurl,
								Boolean simpleMode) {
		this.listView = listView;
		this.posts = new ArrayList<>();
		this.context = context;
		this.baseurl = baseurl;
		this.simpleMode = simpleMode;

		//We'll assume that sitenames don't contain http. Only sitesnames are accepted by the JetPack API.
		if (!baseurl.startsWith("http"))
			this.provider = new JetPackProvider();
		else
			this.provider = new JsonApiProvider();
	}

	public interface ListListener {
        void completedWithPosts();
    }

    public void setListener(ListListener listener){
        this.listener = listener;
    }

    public void completedWithPosts(){
        if (this.listener != null) listener.completedWithPosts();
    }

}
